package cn.kerui;

import cn.kerui.common.framework.util.spring.annotation.EnableInjectSpringContext;
import cn.kerui.common.oss.annotation.EnableFileStorage;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author yangkai
 * @Version 1.0
 */
@SpringBootApplication(proxyBeanMethods = false)
@MapperScan(basePackages = {"cn.kerui.*.mapper", "cn.kerui.*.*.mapper"})
@EnableFeignClients
@EnableDiscoveryClient
@EnableInjectSpringContext
@EnableFileStorage
public class ManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManageApplication.class, args);
    }
}
