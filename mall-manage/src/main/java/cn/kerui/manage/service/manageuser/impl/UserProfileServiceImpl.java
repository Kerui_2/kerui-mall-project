package cn.kerui.manage.service.manageuser.impl;

import cn.kerui.common.enums.AuthExceptionMsg;
import cn.kerui.common.exception.AuthException;
import cn.kerui.common.framework.util.validation.Assert;
import cn.kerui.manage.service.manageuser.ManageUserService;
import cn.kerui.manage.service.manageuser.UserProfileService;
import cn.kerui.repos.entities.manageuser.ManageUserInfo;
import cn.kerui.repos.mapper.manageuser.ManageUserInfoMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * <p> 个人中心业务实现层 </p>
 * <p>创建于 2024/1/27 14:26 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Service
@Slf4j
@AllArgsConstructor
public class UserProfileServiceImpl implements UserProfileService {

    private ManageUserInfoMapper userInfoMapper;

    private ManageUserService userService;


    /**
     * 修改个人信息
     * @param info
     * @return
     */
    @Override
    public Integer updateUserInfo(ManageUserInfo info) {
        // 校验邮箱是否唯一
        Optional.ofNullable(info.getEmail())
                .ifPresent(email -> Assert.isFalse(userService.checkEmailUnique(info), () -> new AuthException(AuthExceptionMsg.EMAIL_EXIST)));

        // 校验手机号是否唯一
        Optional.of(info.getPhone())
                .ifPresent(phone -> Assert.isFalse(userService.checkPhoneUnique(info), () -> new AuthException(AuthExceptionMsg.PHONE_EXIST)));

        // 更新用户信息
        return userInfoMapper.updateById(info);
    }



}
