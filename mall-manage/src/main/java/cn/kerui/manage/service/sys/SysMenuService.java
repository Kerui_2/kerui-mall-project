package cn.kerui.manage.service.sys;

import cn.kerui.manage.vo.sys.RouterVo;
import cn.kerui.manage.vo.sys.SysMenuVo;
import cn.kerui.repos.entities.sys.SysMenu;

import java.util.List;

/**
 * <p> 菜单管理业务层接口 </p>
 * <p>创建于 2024/1/8 15:36 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
public interface SysMenuService {

    /**
     * 查询菜单列表
     * @return
     */
    List<SysMenu> getMenu();

    /**
     * 获取用户菜单
     * @param userId
     * @return
     */
    List<RouterVo> getRouters(Long userId);

    /**
     * 查询所有菜单列表 (平铺返回)
     * @return
     */
    List<SysMenu> getMenuList();

    /**
     * 添加菜单信息
     * @param sysMenuVo
     */
    void addMenu(SysMenuVo sysMenuVo);
}
