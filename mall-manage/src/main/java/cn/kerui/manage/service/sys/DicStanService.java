package cn.kerui.manage.service.sys;

import cn.kerui.manage.vo.sys.CataLogVO;
import cn.kerui.repos.entities.sys.SysDicCatalog;
import cn.kerui.repos.entities.sys.SysDicStan;

import java.util.List;
import java.util.Map;

/**
 * <p> 字典相关业务层 </p>
 * <p>创建于 2024/1/12 14:58 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
public interface DicStanService {


    /**
     * 查询字典相关目录列表
     */
    List<CataLogVO> getCatalogList();

    /**
     * 通过目录ID获取字典列表
     * @param dicCode
     * @return
     */
    List<SysDicStan> getDicStanByCatalogId(String dicCode);

    /**
     * 批量获取字典信息
     * @param dicCodes 多个字典编码用逗号分割
     */
    Map<String, List<SysDicStan>> getDicStanBatch(String dicCodes);

    /**
     * 删除字典目录
     * @param dicCode
     */
    void deleteDicCatalog(String dicCode);

    /**
     * 插入字典目录
     * @param sysDicCatalog 字典目录实体
     */
    void insertDicCatalog(SysDicCatalog sysDicCatalog);

    /**
     * 修改字典目录
     * @param sysDicCatalog
     */
    void updateDicCatalog(SysDicCatalog sysDicCatalog);

    /**
     * 删除字典条目
     * @param id
     */
    void deleteDicStan(Long id);

    /**
     * 新增字典条目
     * @param sysDicStan
     */
    void insertDicStan(SysDicStan sysDicStan);

    /**
     * 更新字典条目
     * @param sysDicStan
     */
    void updateDicStan(SysDicStan sysDicStan);
}
