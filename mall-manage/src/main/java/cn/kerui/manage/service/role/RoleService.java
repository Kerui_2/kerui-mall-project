package cn.kerui.manage.service.role;

import cn.kerui.common.core.PageResult;
import cn.kerui.common.core.domain.CommonResult;
import cn.kerui.manage.vo.role.PageRoleModel;
import cn.kerui.manage.vo.role.RolePermsVo;
import cn.kerui.manage.vo.role.RoleVo;
import cn.kerui.repos.entities.manage.ManageRole;

/**
 * 角色业务层
 * <p>创建于 2024/2/6 20:05 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
public interface RoleService {

    /**
     * 分页查询角色信息
     * @param model
     * @return
     */
    PageResult<RoleVo> page(PageRoleModel model);

    /**
     * 创建角色信息
     * @param role
     * @return
     */
    CommonResult<Integer> save(ManageRole role);

    /**
     * 更新角色信息
     * @param role
     * @return
     */
    CommonResult<Integer> update(ManageRole role);

    /**
     * 添加用户权限
     * @param vo
     * @return
     */
    CommonResult savePerms(RolePermsVo vo);

    /**
     * 删除角色权限
     * @param roleId
     * @param permsId
     * @return
     */
    CommonResult deletePerms(Long roleId, Long menuId);
}
