package cn.kerui.manage.service.manageuser;

import cn.kerui.manage.vo.manageuser.CreateUserModel;
import cn.kerui.manage.vo.manageuser.UserInfoVo;
import cn.kerui.repos.entities.manageuser.ManageUserInfo;

/**
 * <p> 管理端用户业务层接口 </p>
 * <p>创建于 2023/11/23 14:28 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
public interface ManageUserService {

    /**
     * 创建用户
     * @param model
     */
    void createUser(CreateUserModel model);


    /**
     * 获取用户信息
     * @param userId
     * @return
     */
    UserInfoVo getUserInfo(Long userId);

    /**
     * 根据用户Id获取用户信息
     * @param userId
     * @return
     */
    ManageUserInfo getUserInfoByUserId(Long userId);


    /**
     * 校验邮箱是否唯一
     * @param info 用户信息
     * @return
     */
    boolean checkEmailUnique(ManageUserInfo info);

    /**
     * 校验手机号码是否唯一
     * @param info 用户信息
     * @return
     */
    boolean checkPhoneUnique(ManageUserInfo info);
}
