package cn.kerui.manage.service.manageuser;

import cn.kerui.repos.entities.manageuser.ManageUserInfo;

/**
 * <p> 个人中心业务层 </p>
 * <p>创建于 2024/1/27 14:24 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
public interface UserProfileService {

    /**
     * 个人中心修改用户基础信息
     * @param info
     * @return
     */
    Integer updateUserInfo(ManageUserInfo info);
}
