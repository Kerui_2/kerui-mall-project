package cn.kerui.manage.mapper.role;

import cn.kerui.manage.vo.role.PageRoleModel;
import cn.kerui.manage.vo.role.RoleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色扩展数据层
 * <p>创建于 2024/2/6 20:13 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Mapper
public interface SysRoleMapper {

    /**
     * 查询角色以及权限列表
     *
     * @param pageRoleModel
     * @return
     */
    List<RoleVo> selectRoleVoList(@Param("pageRoleModel") PageRoleModel pageRoleModel);
}
