package cn.kerui.manage.constants;

import cn.kerui.common.enums.IExceptionMsg;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p> 管理端异常枚举 </p>
 * Code 范围 ： 30000 - 39999
 * <p>创建于 2024/1/25 15:40 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@AllArgsConstructor
public enum ManageExceptionMsg implements IExceptionMsg {

    // 字典相关异常
    DIC_NOT_EXIST(30001, "字典不存在"),
    DIC_CATALOG_NOT_EXIST(30002, "字典目录不存在"),
    DIC_STAN_NOT_EXIST(30003, "字典条目不存在"),
    DIC_STAN_EXIST(30004, "字典条目已存在"),
    DIC_CATALOG_EXIST(30005, "字典目录已存在"),
    DIC_CATALOG_NOT_EMPTY(30006, "字典目录不为空"),
    ;

    private Integer code;

    private String msg;

    @Override
    public Integer code() {
        return code;
    }

    @Override
    public String message() {
        return msg;
    }
}
