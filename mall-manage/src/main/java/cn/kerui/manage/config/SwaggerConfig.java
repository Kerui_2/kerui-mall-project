package cn.kerui.manage.config;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.kerui.common.framework.util.lang.StringUtil;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.parameters.HeaderParameter;
import org.springdoc.core.customizers.GlobalOpenApiCustomizer;
import org.springdoc.core.customizers.GlobalOperationCustomizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

/**
 * <p>
 * swagger配置类
 * </p>
 * <p>
 * 创建于 2023/11/30 12:30
 * </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
//@Configuration
public class SwaggerConfig {

    @Value("${spring.application.name}")
    private String serviceName;

    @Bean
    public GlobalOpenApiCustomizer globalOpenApiCustomizer() {
        Contact contact = new Contact().name("杨凯") // 作者名称
            .email("18092119190@163.com") // 作者邮箱
            .url("https://kenyang.vercel.app/"); // 作者主页

        Info info = new Info().title(StringUtil.format("{} 服务 Swagger接口文档", serviceName)) // 文档标题
            .description("微服务后端API接口文档") // 文档描述
            .license(new License().name("杨先生的小破站").url("https://kenyang.vercel.app/")) // 授权许可信息
            .summary(StringUtil.format("build Time : {}", LocalDateTimeUtil.formatNormal(LocalDateTimeUtil.now())))
            .contact(contact); // 联系人信息

        return openApi -> openApi.info(info);
    }

    @Bean
    public GlobalOperationCustomizer globalOperationCustomizer() {
        return (operation, handler) -> {
            operation.addParametersItem(new HeaderParameter().name("X-User-Id").description("用户ID").required(true));
            return operation;
        };
    }

}
