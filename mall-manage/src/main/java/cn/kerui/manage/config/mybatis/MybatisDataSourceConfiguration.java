package cn.kerui.manage.config.mybatis;

import cn.kerui.common.mybatis.plus.handler.BaseModelHandler;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName MybatisDataSourceConfiguration.java
 * 
 * @createTime 2023年09月28日 10:46:00
 */
@Configuration
@AutoConfigureAfter(DataSourceConfiguration.class)
@Slf4j
public class MybatisDataSourceConfiguration {

    @Autowired
    @Qualifier("masterDb")
    private DataSource masterDataSource;


    @Bean(name = "masterSqlSessionFactory")
    public SqlSessionFactory sqlSessionFactoryBean() throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(masterDataSource);
        //指定mapper xml目录
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Set<Resource> sets = new LinkedHashSet<>(16);
        try {
            sets.addAll(Arrays.asList(resolver.getResources("classpath*:mapper/*.xml")));
            sets.addAll(Arrays.asList(resolver.getResources("classpath*:mapper/*/*.xml")));
        } catch (IOException e) {
            log.error("未找到mapper文件路径");
        }
        sqlSessionFactoryBean.setMapperLocations(sets.toArray(new Resource[0]));
        sqlSessionFactoryBean.setGlobalConfig(globalConfig());
        sqlSessionFactoryBean.setTypeHandlersPackage("cn.kerui.common.mybatis.handler");
//        sqlSessionFactoryBean.setPlugins(mybatisPlusInterceptor());
        MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
        mybatisConfiguration.setLogImpl(StdOutImpl.class);
        mybatisConfiguration.setMapUnderscoreToCamelCase(true);
        sqlSessionFactoryBean.setConfiguration(mybatisConfiguration);

        return sqlSessionFactoryBean.getObject();

    }


    @Primary
    @Bean(name = "masterTransactionManager")
    public DataSourceTransactionManager pgTransactionManager(@Qualifier("masterDb") DataSource pgDataSource) {
        return new DataSourceTransactionManager(pgDataSource);
    }

    @Primary
    @Bean(name = "masterSqlSessionTemplate")
    public SqlSessionTemplate pgSqlSessionTemplate(@Qualifier("masterSqlSessionFactory") SqlSessionFactory pgSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(pgSqlSessionFactory);
    }

    private GlobalConfig globalConfig() {
        return new GlobalConfig()
               .setBanner(false)
               .setMetaObjectHandler(new BaseModelHandler());
    }

}
