package cn.kerui.manage.controller.sys;

import cn.kerui.manage.service.sys.DicStanService;
import cn.kerui.manage.vo.sys.CataLogVO;
import cn.kerui.repos.entities.sys.SysDicCatalog;
import cn.kerui.repos.entities.sys.SysDicStan;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 字典管理
 * <p>创建于 2024/1/12 14:56 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/sys/dic")
@AllArgsConstructor
@Tag(name = "字典相关接口")
public class DicStanController {

    private DicStanService dicStanService;

    /**
     * 获取字典目录
     * @return 字典目录集合
     */
    @GetMapping("/catalog")
    public List<CataLogVO> getCatalogList() {
        return dicStanService.getCatalogList();
    }


    /**
     * 通过目录ID获取字典数据
     * @param dicCode  字典编码
     * @return
     */
    @GetMapping("/dic")
    public List<SysDicStan> getDicStanByCatalogId(@RequestParam("dicCode") String dicCode) {
        return dicStanService.getDicStanByCatalogId(dicCode);
    }

    /**
     * 批量获取字典信息
     * @param dicCodes 多个字典编码用逗号分割
     */
    @GetMapping("/dic/batch")
    public Map<String, List<SysDicStan>> getDicStanBatch(@RequestParam("dicCodes") String dicCodes) {
        return dicStanService.getDicStanBatch(dicCodes);
    }

    /**
     * 删除字典目录
     * @param dicCode
     */
    @DeleteMapping("/catalog/{dicCode}")
    public void deleteDicCatalog(@PathVariable("dicCode") String dicCode) {
        dicStanService.deleteDicCatalog(dicCode);
    }

    /**
     * 新增字典目录
     * @param sysDicCatalog
     */
    @PostMapping("/catalog")
    public void insertDicCatalog(@RequestBody SysDicCatalog sysDicCatalog) {
        dicStanService.insertDicCatalog(sysDicCatalog);
    }

    /**
     * 更新字典目录
     * @param sysDicCatalog
     */
    @PutMapping("/catalog")
    public void updateDicCatalog(@RequestBody SysDicCatalog sysDicCatalog) {
        dicStanService.updateDicCatalog(sysDicCatalog);
    }

    /**
     * 删除字典条目
     * @param id
     */
    @DeleteMapping("/dic/{id}")
    public void deleteDicStan(@PathVariable("id") Long id) {
        dicStanService.deleteDicStan(id);
    }

    /**
     * 新增字典条目
     * @param sysDicStan
     */
    @PostMapping("/dic")
    public void insertDicStan(@RequestBody SysDicStan sysDicStan) {
        dicStanService.insertDicStan(sysDicStan);
    }

    /**
     * 更新字典条目
     * @param sysDicStan 字典实体
     */
    @PutMapping("/dic")
    public void updateDicStan(@RequestBody SysDicStan sysDicStan) {
        dicStanService.updateDicStan(sysDicStan);
    }
}
