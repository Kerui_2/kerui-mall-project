package cn.kerui.manage.controller.manageuser;

import cn.kerui.common.core.domain.CommonResult;
import cn.kerui.common.helper.RequestHelper;
import cn.kerui.manage.service.manageuser.ManageUserService;
import cn.kerui.manage.vo.manageuser.CreateUserModel;
import cn.kerui.manage.vo.manageuser.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p> 管理端用户控制层 </p>
 * <p>创建于 2023/11/23 14:19 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/system/user")
public class ManageUserController {

    @Autowired
    private ManageUserService manageUserService;

    /**
     * 创建管理端用户
     *
     * @param model 创建管理端用户对象
     * @return {@link CreateUserModel}
     */
    @PostMapping("/create")
    public CommonResult createUser(@RequestBody CreateUserModel model) {
        manageUserService.createUser(model);
        return CommonResult.okResult();
    }

    /**
     * 获取用户信息
     * @return
     */
    @GetMapping("/getInfo")
    public UserInfoVo getUserInfo() {
        return manageUserService.getUserInfo(RequestHelper.getUserId());
    }


}
