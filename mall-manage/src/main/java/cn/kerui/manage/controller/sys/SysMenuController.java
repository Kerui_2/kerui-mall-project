package cn.kerui.manage.controller.sys;

import cn.kerui.common.helper.RequestHelper;
import cn.kerui.manage.service.sys.SysMenuService;
import cn.kerui.manage.vo.sys.RouterVo;
import cn.kerui.manage.vo.sys.SysMenuVo;
import cn.kerui.repos.entities.sys.SysMenu;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p> 系统菜单控制层 </p>
 * <p>创建于 2024/1/8 14:09 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/sys/menu")
@AllArgsConstructor
@Slf4j
public class SysMenuController {

    private SysMenuService sysMenuService;


    /**
     * 获取用户路由地址
     * @return 路由列表
     */
    @GetMapping("/getRouters")
    public List<RouterVo> getRouters() {
         return sysMenuService.getRouters(RequestHelper.getUserId());
    }


    /**
     * 查询菜单列表
     * @return 菜单列表
     */
    @GetMapping()
    public List<SysMenu> getMenu() {
        return sysMenuService.getMenu();
    }

    /**
     * 查询菜单列表（平铺)
     * @return
     */
    @GetMapping("/list")
    public List<SysMenu> getMenuList() {
        return sysMenuService.getMenuList();
    }

    /**
     * 新增菜单
     */
    @PostMapping("/add")
    public void addMenu(@RequestBody SysMenuVo sysMenuVo) {
         sysMenuService.addMenu(sysMenuVo);
    }

    /**
     * 删除菜单
     * @param id
     */
    @PostMapping("del/{id}")
    public void deleteMenu(@PathVariable Long id) {
        return;
    }
}
