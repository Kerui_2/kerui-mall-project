package cn.kerui.manage.controller.manageuser;

import cn.hutool.core.bean.BeanUtil;
import cn.kerui.common.core.domain.CommonResult;
import cn.kerui.common.helper.RequestHelper;
import cn.kerui.manage.service.manageuser.ManageUserService;
import cn.kerui.manage.service.manageuser.UserProfileService;
import cn.kerui.manage.vo.manageuser.ProfileVo;
import cn.kerui.manage.vo.manageuser.UserProfileModel;
import cn.kerui.repos.entities.manageuser.ManageUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> 个人中心相关 </p>
 * <p>创建于 2024/1/27 12:08 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/user/profile")
public class UserProfileController {

    @Autowired
    private UserProfileService userProfileService;

    private ManageUserService userService;

    /**
     * 获取个人信息
     * @return
     */
    public ProfileVo profile() {
        ManageUserInfo userInfoByUserId = userService.getUserInfoByUserId(RequestHelper.getUserId());
        ProfileVo profileVo = new ProfileVo();
        profileVo.setUserInfo(userInfoByUserId);
        return profileVo;
    }

    /**
     * 修改个人基础信息
     */
    @PutMapping
    public CommonResult<Void> updateProfile(@RequestBody UserProfileModel model) {
        ManageUserInfo manageUserInfo = BeanUtil.copyProperties(model, ManageUserInfo.class);
        if (userProfileService.updateUserInfo(manageUserInfo) > 0) {
            return CommonResult.okResult();
        }
        return CommonResult.error();
    }
}
