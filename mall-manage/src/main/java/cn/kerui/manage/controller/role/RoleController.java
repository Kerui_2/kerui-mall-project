package cn.kerui.manage.controller.role;

import cn.kerui.common.core.PageResult;
import cn.kerui.common.core.domain.CommonResult;
import cn.kerui.manage.service.role.RoleService;
import cn.kerui.manage.vo.role.PageRoleModel;
import cn.kerui.manage.vo.role.RolePermsVo;
import cn.kerui.manage.vo.role.RoleVo;
import cn.kerui.repos.entities.manage.ManageRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 角色相关控制层
 * <p>创建于 2024/2/6 11:24 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/sys/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 分页查询角色信息
     *
      * @param pageRoleModel
     * @return {@link PageResult}<{@link RoleVo}>
     */
    @GetMapping("/page")
    public PageResult<RoleVo> page(@RequestBody PageRoleModel pageRoleModel) {
        return roleService.page(pageRoleModel);
    }

    /**
     * 创建角色信息
     * @return
     */
    @PostMapping("/save")
    public CommonResult<Integer> saveRole(@RequestBody ManageRole role) {
        return roleService.save(role);
    }

    /**
     * 更新角色信息
     * @param role
     * @return
     */
    @PostMapping("/update")
    public CommonResult<Integer> updateRole(@RequestBody ManageRole role) {
        return roleService.update(role);
    }

    /**
     * 增加角色权限
     * @param vo
     * @return
     */
    @PostMapping("/add/perms")
    public CommonResult<?> addRolePerms(@RequestBody RolePermsVo vo) {
        return roleService.savePerms(vo);
    }


    /**
     * 删除角色权限
     *
     * @param id
     * @param menuId
     * @return
     */
    @PostMapping("/delete/perms/{roleId}/{menuId}")
    public CommonResult<?> deleteRolePerms(@PathVariable Long roleId,
                                        @PathVariable Long menuId) {
        return roleService.deletePerms(roleId, menuId);
    }
}
