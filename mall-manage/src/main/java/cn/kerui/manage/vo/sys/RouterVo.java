package cn.kerui.manage.vo.sys;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * <p> 路由配置信息 </p>
 * <p>创建于 2024/1/9 19:46 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class RouterVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 路由名字
     */
    private String name;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 是否隐藏路由， 当设置true的时候该路由不会在侧边栏出现
     */
    private boolean hidden;

    /**
     * 重定向地址 当设置noRedirect的时候该路由在面包屑导航中不可被点击
     */
    private String redirect;

    /**
     * 组件地址
     */
    private String component;

    /**
     * 路由参数: 如 {"id": 1, "name" = "test"}
     */
    private String query;

    /**
     * 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
     */
    private Boolean alwaysShow;

    /**
     * 其他扩展信息
     */
    private MetaVo meta;

    /**
     * 子路由集合
     */
    private List<RouterVo> children;
}
