package cn.kerui.manage.vo.manageuser;

import cn.kerui.repos.entities.manageuser.ManageUserInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.Set;

/**
 * <p> 用户信息 </p>
 * <p>创建于 2024/1/10 17:21 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class UserInfoVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 用户信息
     */
    private ManageUserInfo user;

    /**
     * 用户角色集合
     */
    private Set<String> roles;

    /**
     * 菜单权限列表集合
     */
    private Set<String> permissions;
}
