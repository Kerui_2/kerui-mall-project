package cn.kerui.manage.vo.role;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 角色权限信息
 * <p>创建于 2024/4/24 17:30 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since
 */
@Getter
@Setter
@NoArgsConstructor
public class RolePermsVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 2132109821894L;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 菜单ID
     */
    private List<Long> menuId;
}
