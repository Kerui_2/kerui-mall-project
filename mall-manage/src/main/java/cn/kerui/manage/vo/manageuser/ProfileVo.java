package cn.kerui.manage.vo.manageuser;

import cn.kerui.repos.entities.manageuser.ManageUserInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> 个人中心用户信息 </p>
 * <p>创建于 2024/1/27 15:43 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@NoArgsConstructor
@Getter
@Setter
public class ProfileVo {

    /**
     * 用户信息
     */
    private ManageUserInfo userInfo;


}
