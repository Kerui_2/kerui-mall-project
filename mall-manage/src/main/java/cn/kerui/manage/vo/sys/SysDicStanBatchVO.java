package cn.kerui.manage.vo.sys;

import cn.kerui.repos.entities.sys.SysDicStan;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * <p> 批量获取字典信息列表 </p>
 * <p>创建于 2024/1/25 14:30 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class SysDicStanBatchVO implements Serializable {

    private String dicCode;

    private List<SysDicStan> dicStanList;
}
