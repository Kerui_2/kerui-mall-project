package cn.kerui.manage.vo.manageuser;

import cn.kerui.common.core.BaseModel;
import cn.kerui.common.core.xss.Xss;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> 用户信息实体 </p>
 * <p>创建于 2024/1/27 11:39 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserProfileModel extends BaseModel {

    /**
     * 用户Id
     */
    private Long id;

    /**
     * 用户昵称
     */
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过{max}个字符")
    @Xss(message = "用户昵称存在特殊字符")
    private String nickName;

    /**
     * 用户邮箱
     */
    @Size(min = 0, max = 50, message = "邮箱长度不能超过{max}个字符")
    @Email(message = "邮箱格式不正确")
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 性别 1 男 2 女 0 未知
     */
    private String sex;

}
