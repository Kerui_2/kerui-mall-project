package cn.kerui.manage.vo.sys;

import cn.kerui.repos.entities.sys.SysDicCatalog;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> 目录相关VO </p>
 * <p>创建于 2024/1/17 16:44 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class CataLogVO {

    /**
     * 主键ID
     */
    private String id;

    /**
     * 目录名称
     */
    private String name;

    /**
     * 目录编码
     */
    private String code;

    /**
     * 子节点数量
     */
    private Integer childNum;

    /**
     * 排序
     */
    private Integer sort;


    public CataLogVO (SysDicCatalog catalog) {
        this.id = catalog.getDicCode();
        this.code = catalog.getDicCode();
        this.name = catalog.getDicName();
        this.sort = catalog.getSort();
    }
}
