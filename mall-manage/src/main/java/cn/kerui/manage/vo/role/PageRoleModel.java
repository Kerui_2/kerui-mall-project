package cn.kerui.manage.vo.role;

import cn.kerui.common.core.BaseQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * 分页查询角色入参
 * <p>创建于 2024/2/6 19:57 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class PageRoleModel extends BaseQuery implements Serializable {

    @Serial
    private static final long serialVersionUID = 289783650601432424L;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 权限字符
     */
    private String permission;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 创建开始时间
     */
    private LocalDate startDate;

    /**
     * 创建结束时间
     */
    private LocalDate endDate;
}
