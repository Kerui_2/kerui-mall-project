package cn.kerui.manage.vo.sys;

import cn.kerui.repos.entities.sys.SysMenu;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p> 查询菜单列表出参 </p>
 * <p>创建于 2024/1/8 15:32 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysMenuVo extends SysMenu implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

}
