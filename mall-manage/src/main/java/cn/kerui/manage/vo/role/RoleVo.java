package cn.kerui.manage.vo.role;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 角色列表出参
 * <p>创建于 2024/2/6 19:56 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class RoleVo {

    /**
     * 角色ID
     */
    private Long id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 排序信息
     */
    private Integer sort;

    /**
     * 子角色
     */
    @TableField(exist = false)
    private List<RoleVo> childRoles;
}
