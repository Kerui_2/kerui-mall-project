# 凯瑞玺越商城



## 项目简介



### 项目介绍

该项目包含了管理端、小程序端后台程序。本项目为前后端分离项目，后端为微服务模块，使用主流技术`Spring-Cloud-Alibaba`, `Nacos`, `Spring-Bus`等。使用技术栈在文章后面有详细介绍。

**项目系统需求**

- JDK >= 17  [JDK1.8 到 JDK17的主要改动](https://blog.csdn.net/CYK_byte/article/details/135329731)
- MySQL  >= 5.7 
- Maven >= 3.0





### 技术选型

1. 系统环境
- Java EE 17
- Maven 3
- MySql 5.7
2. 主框架
- Spring Boot 3.1.x [Spring Boot 3.0 新特性](https://blog.csdn.net/goPlayJava/article/details/136808914)
- Spring Cloud 2022.0.4
- Spring OpenFeign 4.0.x
3. 持久层
- Apache Mybatis 3.5.X
- Alibaba Druid 1.2.x
- Hibernate Validation 8.0.x
4. 视图层
- Vue 3.x
- Axios 0.21x
- Element 2.15.x



### 第三方中间件

> - OSS： [x-file-storage](https://x-file-storage.xuyanwu.cn/#/)
> - SSO： [Sa-Token](https://sa-token.cc/index.html)
> - Redis: Jedis 4.0
> - MQ: RocketMQ (暂未集成)
> - SMS: [SMS4J](https://sms4j.com/) (暂未集成)
> - XXL-JOB: [XXL-JOB](https://www.xuxueli.com/xxl-job/) (暂未集成)
> - 未完待续........




## 环境部署

### 环境配置
> JDK >= 17 
> 
> Mysql >= 5.7
> 
> Redis >= 3.0
> 
> Maven >= 3.0
>
> Nacos >= 2.0.4
> 
> Nginx 
> 
> Docker / K8s

### 后端运行配置/功能配置 

> EasyCode
> 
> Host文件配置
> 
> 未完待续......


## 功能配置

- 用户管理: 用户是系统的操作者，用户管理主要实现用户登录、退出、用户管理、角色管理、权限管理
- 系统管理：系统管理主要是对系统信息的配置管理，包括字典管理、系统参数管理、OSS文件配置管理（注意:该模块适用于系统管理相关，不对外开放，仅支持系统管理员获取权限)
- 菜单管理: 菜单管理主要是配置系统菜单，菜单操作权限以及按钮权限标识等
- 日志管理：包括登陆日志，操作日志，异常日志等
- 消息管理：包括短信配置管理，短信模版配置，短信发送记录，异常推送，消息告警记录等
- 公告管理：系统通知公告信息发布和维护，包括公告的查看、发布、修改、删除、导出、导入等
- 定时任务：定时任务管理，主要用于系统定时任务配置，定时任务日志管理，定时任务日志的查看、删除、导出、导入等
- 商家管理：商家主要是配置商家的信息，包括商家的资质信息、商家的资质审核、商家的资质修改、商家的资质删除、商家的资质查看、商家的资质导出


## 项目架构

### 后端架构

```text
cn.kerui    
├── mall-gateway         // 网关模块 [8080]
├── mall-auth            // 认证中心 [8081]
├── mall-common             // 通用模块
│       └── mall-feign-api                           // RPC远程调用模块
│       └── mall-common-core                         // 通用公共模块
├── mall-repos           //  实体模块
├── cloud-common         // 公共框架模块
│       └── cloud-common-core                         // 核心模块
│       └── cloud-common-doc                          // 接口文档模块
│       └── cloud-common-exception                    // 异常模块
│       └── cloud-common-framework                    // 框架模块
│       └── cloud-common-log                          // 日志记录、配置
│       └── cloud-common-mybatis                      // 数据源配置
│       └── cloud-common-oss                          // 文件存储
│       └── cloud-common-redis                        // 缓存服务
│       └── cloud-common-satoken                      // sa-token配置
│       └── cloud-common-web                          // web模块
├── mall-minapp          // 小程序入口 [8082]
├── mall-manage          // 后台管理端入口 [8083]
├── mall-product         // 商品模块 [8084]
├── mall-order           // 订单模块 [8085]
├── mall-pay             // 支付模块 [8086]
├──pom.xml                // 公共依赖管理
```
