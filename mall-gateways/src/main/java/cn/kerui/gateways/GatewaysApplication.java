package cn.kerui.gateways;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName GatewaysApplication.java
 * 
 * @createTime 2023年10月17日 19:40:00
 */
@SpringBootApplication(scanBasePackages = {"cn.kerui"})
@EnableDiscoveryClient
public class GatewaysApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewaysApplication.class, args);
    }
}
