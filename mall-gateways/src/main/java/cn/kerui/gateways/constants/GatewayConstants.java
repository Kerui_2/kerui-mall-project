package cn.kerui.gateways.constants;

/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName GatewayConstatns.java
 * 
 * @createTime 2023年10月18日 09:10:00
 */
public  class GatewayConstants {

    public static final String TRACE_ID = "traceId";

    public static final String REQUEST_NO = "request-no";

    public static final String SOURCE_IP = "source-ip";
}
