package cn.kerui.product.controller;

import cn.kerui.common.core.domain.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName OrderNacosController.java
 * 
 * @createTime 2023年06月13日 17:07:00
 */
@RestController("/product")
@Slf4j
public class OrderNacosController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/consumer/payment/nacos/{id}")
    public CommonResult paymentInfo(@PathVariable("id") Long id) {
        log.info("测试feign");
        return CommonResult.okResult("请求成功");
    }

    @RequestMapping("/test")
    public CommonResult test() {
        log.error("测试feign");
        return CommonResult.okResult("测试feign成功");
    }
}
