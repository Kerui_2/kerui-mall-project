package cn.kerui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName ConsumerNacosOrder83Application.java
 * 
 * @createTime 2023年06月13日 16:34:00
 */

@SpringBootApplication
@EnableDiscoveryClient
public class ProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class, args);
    }
}
