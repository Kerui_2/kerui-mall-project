# 使用一个基础的 Java 镜像（根据自己项目中使用的是什么jdk版本设置，用于拉取执行jar包的jdk环境）
FROM openjdk:17-jdk-slim

# 指定工作目录
VOLUME /tmp

# 复制应用程序的 JAR 文件到镜像中（需要是相对路径）
COPY mall-manage/target/mall-manage-1.0.0.jar app.jar

# 定义容器启动时执行的命令
ENTRYPOINT ["java","-jar","app.jar","&"]


CMD java -jar app.jar &
