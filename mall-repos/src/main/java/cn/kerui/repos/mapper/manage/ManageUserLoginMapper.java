package cn.kerui.repos.mapper.manage;

import cn.kerui.repos.entities.manage.ManageUserLogin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangkai
 * @since 2024-01-04
 */
@Repository
public interface ManageUserLoginMapper extends BaseMapper<ManageUserLogin> {

}
