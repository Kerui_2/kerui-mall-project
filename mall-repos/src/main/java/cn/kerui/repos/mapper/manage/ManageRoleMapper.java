package cn.kerui.repos.mapper.manage;

import cn.kerui.repos.entities.manage.ManageRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangkai
 * @since 2024-02-18
 */
@Repository
public interface ManageRoleMapper extends BaseMapper<ManageRole> {

    /**
     * 查询子角色列表
     *
     * @param parentId 父级角色ID
     * @return
     */
    List<ManageRole> selectRoleWithChild(@Param("parentId") Long parentId);
}
