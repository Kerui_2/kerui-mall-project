package cn.kerui.repos.mapper.log;

import cn.kerui.repos.entities.log.UserLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangkai
 * @since 2024-01-04
 */
@Repository
public interface UserLoginLogMapper extends BaseMapper<UserLoginLog> {

}
