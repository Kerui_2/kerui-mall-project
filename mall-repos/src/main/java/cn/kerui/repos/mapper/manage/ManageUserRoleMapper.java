package cn.kerui.repos.mapper.manage;

import cn.kerui.repos.entities.manage.ManageUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangkai
 * @since 2023-11-17
 */
@Repository
public interface ManageUserRoleMapper extends BaseMapper<ManageUserRole> {

}
