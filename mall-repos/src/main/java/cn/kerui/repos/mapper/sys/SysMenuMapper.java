package cn.kerui.repos.mapper.sys;

import cn.kerui.repos.entities.sys.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangkai
 * @since 2024-01-08
 */
@Repository
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
