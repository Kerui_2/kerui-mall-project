package cn.kerui.repos.mapper.manage;

import java.util.List;

import cn.kerui.repos.entities.manage.ManageRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 角色权限中间表(ManageRoleMenu)表数据库访问层
 *
 * @author Ken.Yang
 * @since 2024-06-02 19:18:39
 */
public interface ManageRoleMenuMapper extends BaseMapper<ManageRoleMenu> {

/**
* 批量新增数据（MyBatis原生foreach方法）
*
* @param entities List<ManageRoleMenu> 实例对象列表
* @return 影响行数
*/
int insertBatch(@Param("entities") List<ManageRoleMenu> entities);

/**
* 批量新增或按主键更新数据（MyBatis原生foreach方法）
*
* @param entities List<ManageRoleMenu> 实例对象列表
* @return 影响行数
* @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
*/
int insertOrUpdateBatch(@Param("entities") List<ManageRoleMenu> entities);

}

