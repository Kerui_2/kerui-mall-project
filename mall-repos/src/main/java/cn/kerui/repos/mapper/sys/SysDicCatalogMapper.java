package cn.kerui.repos.mapper.sys;

import cn.kerui.repos.entities.sys.SysDicCatalog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangkai
 * @since 2024-01-12
 */
@Repository
public interface SysDicCatalogMapper extends BaseMapper<SysDicCatalog> {

}
