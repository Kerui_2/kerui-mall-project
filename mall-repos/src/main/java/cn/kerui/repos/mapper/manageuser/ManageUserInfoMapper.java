package cn.kerui.repos.mapper.manageuser;

import cn.kerui.repos.entities.manageuser.ManageUserInfo;
import cn.kerui.repos.entities.sys.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangkai
 * @since 2024-01-10
 */
@Repository
public interface ManageUserInfoMapper extends BaseMapper<ManageUserInfo> {

    /**
     * 获取用户权限信息
     */
    List<SysMenu> getUserPerm(@Param("userId") Long userId);
}
