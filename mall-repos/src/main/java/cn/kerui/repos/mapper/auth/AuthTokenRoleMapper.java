package cn.kerui.repos.mapper.auth;

import cn.kerui.repos.entities.auth.AuthTokenRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangkai
 * @since 2024-01-03
 */
@Repository
public interface AuthTokenRoleMapper extends BaseMapper<AuthTokenRole> {

}
