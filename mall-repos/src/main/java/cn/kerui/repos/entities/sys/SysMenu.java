package cn.kerui.repos.entities.sys;

import cn.kerui.common.core.constant.Constants;
import cn.kerui.common.core.constant.GlobalConstants;
import cn.kerui.common.core.BaseModel;
import cn.kerui.common.framework.util.lang.StringUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 
 *
 * </p>
 *
 * @author yangkai
 * @since 2024-01-08
 */
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@TableName("sys_menu")
@ApiModel(value="SysMenu对象", description="")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysMenu extends BaseModel {

    @Serial
    private static final long serialVersionUID = 2L;

    @ApiModelProperty(value = "菜单ID")
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long menuId;

    @ApiModelProperty(value = "父级菜单ID")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "菜单名称")
    @TableField("menu_name")
    private String menuName;

    @ApiModelProperty(value = "显示顺序")
    @TableField("order_num")
    private Integer orderNum;

    @ApiModelProperty(value = "路由地址")
    @TableField("path")
    private String path;

    @ApiModelProperty(value = "组件路径")
    @TableField("component")
    private String component;

    @ApiModelProperty(value = "路由参数")
    @TableField("query_param")
    private String queryParam;

    @ApiModelProperty(value = "是否外链 1 是 0 否是否为菜单内部跳转")
    @TableField("is_frame")
    private String isFrame;

    @ApiModelProperty(value = "是否缓存 1 缓存 0 不缓存")
    @TableField("is_cache")
    private String isCache;

    @ApiModelProperty(value = "类型 	M 目录 C 菜单 F 按钮")
    @TableField("menu_type")
    private String menuType;

    @ApiModelProperty(value = "显示状态 1 显示 0 隐藏")
    @TableField("state")
    private String state;

    @ApiModelProperty(value = "权限字符串")
    @TableField("perms")
    private String perms;

    @ApiModelProperty(value = "菜单图标")
    @TableField("icon")
    private String icon;

    @ApiModelProperty(value = "扩展数据")
    @TableField("add_info")
    private String addInfo;

    /**
     * 父菜单名称
     */
    @TableField(exist = false)
    private String parentName;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    private List<SysMenu> children = new ArrayList<>();

    /**
     * 是否存在子级结构
     */
    @TableField(exist = false)
    private Boolean hasChildren;

    /**
     * 获取路由地址
     */
    public String getRouterPath() {
        String routerPath = this.path;
        // 内链打开外网方式
        if (getParentId() != 0L && isInnerLink()) {
            routerPath = innerLinkReplaceEach(routerPath);
        }
        // 非外链并且是一级目录（类型为目录）
        if (0L == getParentId() && GlobalConstants.TYPE_DIR.equals(getMenuType())
                && GlobalConstants.NO_FRAME.equals(getIsFrame())) {
            routerPath = "/" + this.path;
        }
        // 非外链并且是一级目录（类型为菜单）
        else if (isMenuFrame()) {
            routerPath = "/";
        }
        return routerPath;
    }


    /**
     * 获取组件信息
     */
    public String getComponentInfo() {
        String component = GlobalConstants.LAYOUT;
        if (StringUtils.isNotEmpty(this.component) && !isMenuFrame()) {
            component = this.component;
        } else if (StringUtils.isEmpty(this.component) && getParentId() != 0L && isInnerLink()) {
            component = GlobalConstants.INNER_LINK;
        } else if (StringUtils.isEmpty(this.component) && isParentView()) {
            component = GlobalConstants.PARENT_VIEW;
        }
        return component;
    }

    /**
     * 获取路由名称
     * @return
     */
    public String getRouteName() {
        String routeName = StringUtils.capitalize(path);
        // 非外链并且是一级目录(类型为目录)
        if (isMenuFrame()) {
            routeName = StringUtils.EMPTY;
        }
        return routeName;
    }

    /**
     * 是否为菜单内部跳转
     * @return
     */
    public boolean isMenuFrame() {
        return getParentId() == 0L && GlobalConstants.TYPE_MENU.equals(menuType) && isFrame.equals(GlobalConstants.NO_FRAME);
    }

    /**
     * 是否为内链组件
     * @return
     */
    public boolean isInnerLink() {
        return isFrame.equals(GlobalConstants.NO_FRAME) && StringUtil.ishttp(path);
    }

    /**
     * 是否为parentView组件
     * @return
     */
    public boolean isParentView() {
        return getParentId() != 0L && GlobalConstants.TYPE_DIR.equals(menuType);
    }

    public static String innerLinkReplaceEach(String path) {
        return StringUtils.replaceEach(path, new String[]{Constants.HTTP, Constants.HTTPS, Constants.WWW, ".", ":"},
                new String[]{"", "", "", "/", "/"});
    }
}
