package cn.kerui.repos.entities.auth;

import cn.kerui.common.core.BaseModel;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
/**
 * <p>
 * 
 *
 * </p>
 *
 * @author yangkai
 * @since 2024-01-03
 */
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@TableName("auth_token_role")
@ApiModel(value="AuthTokenRole对象", description="")
public class AuthTokenRole extends BaseModel {

    @Serial
    private static final long serialVersionUID = 2L;

    @ApiModelProperty(value = "设备类型")
    @TableId(value = "device_type")
    private String deviceType;

    @ApiModelProperty(value = "过期时间")
    @TableField("expir_time")
    private String expirTime;

    @ApiModelProperty(value = "最低活跃频率")
    @TableField("active_time")
    private String activeTime;

    @ApiModelProperty(value = "有效性")
    @TableField("state")
    private Integer state;


}
