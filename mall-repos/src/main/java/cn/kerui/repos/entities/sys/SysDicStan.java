package cn.kerui.repos.entities.sys;

import cn.kerui.common.core.BaseModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;
import java.io.Serial;
/**
 * <p>
 * 
 *
 * </p>
 *
 * @author yangkai
 * @since 2024-01-12
 */
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@TableName("sys_dic_stan")
@ApiModel(value="SysDicStan对象", description="")
public class SysDicStan extends BaseModel {

    @Serial
    private static final long serialVersionUID = 2L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "字典编码")
    @TableField("dic_code")
    private String dicCode;

    @ApiModelProperty(value = "字典显示名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "字典值")
    @TableField("value")
    private String value;

    @ApiModelProperty(value = "显示排序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "字典显示样式")
    @TableField("list_class")
    private String listClass;

    @ApiModelProperty(value = "有效性 0 无效 1 有效")
    @TableField("state")
    private Integer state;

    @ApiModelProperty(value = "扩展数据")
    @TableField("add_info")
    private String addInfo;


}
