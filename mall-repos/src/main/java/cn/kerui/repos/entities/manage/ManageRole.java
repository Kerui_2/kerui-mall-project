package cn.kerui.repos.entities.manage;

import cn.kerui.common.core.BaseModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
/**
 * <p>
 * 
 *
 * </p>
 *
 * @author yangkai
 * @since 2024-02-18
 */
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@TableName("manage_role")
@ApiModel(value="ManageRole对象", description="")
public class ManageRole extends BaseModel {

    @Serial
    private static final long serialVersionUID = 2L;

    @ApiModelProperty(value = "角色Id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private Long id;

    @ApiModelProperty(value = "角色名称")
    @TableField("role_name")
    private String roleName;

    @ApiModelProperty(value = "父级角色ID")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "排序")
    @TableField("sort")
    private Integer sort;


}
