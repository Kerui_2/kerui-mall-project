package cn.kerui.repos.entities;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName SecondKill.java
 * 
 * @createTime 2023年09月28日 15:39:00
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@TableName(value = "second_kill")
public class SecondKill {

    private Integer skgId;

    private Integer number;
}
