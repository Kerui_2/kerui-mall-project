package cn.kerui.repos.entities.manageuser;

import cn.kerui.common.core.BaseModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;
import java.io.Serial;
/**
 * <p>
 * 
 *
 * </p>
 *
 * @author yangkai
 * @since 2024-01-10
 */
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@TableName("manage_user_info")
@ApiModel(value="ManageUserInfo对象", description="")
public class ManageUserInfo extends BaseModel {

    @Serial
    private static final long serialVersionUID = 2L;

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "用户名")
    @TableField("user_name")
    private String userName;

    @ApiModelProperty(value = "用户昵称")
    @TableField("nick_name")
    private String nickName;

    @ApiModelProperty(value = "用户邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty(value = "手机号码")
    @TableField("phone")
    private String phone;

    @ApiModelProperty(value = "用户性别 0 未知 1 男 2 女 dic_sex")
    @TableField("sex")
    private Integer sex;

    @ApiModelProperty(value = "用户头像")
    @TableField("avater")
    private String avater;

    @ApiModelProperty(value = "有效性 0 无效 1 有效")
    @TableField("state")
    private Integer state;

    @ApiModelProperty(value = "扩展数据")
    @TableField("add_info")
    private String addInfo;


}
