package cn.kerui.repos.entities.manage;

import cn.kerui.common.core.BaseModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
/**
 * <p>
 * 
 *
 * </p>
 *
 * @author yangkai
 * @since 2024-04-24
 */
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@TableName("manage_role_menu")
@ApiModel(value="ManageRoleMenu对象", description="")
public class ManageRoleMenu extends BaseModel {

    @Serial
    private static final long serialVersionUID = 2L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "角色ID")
    @TableField("role_id")
    private Long roleId;

    @ApiModelProperty(value = "功能ID")
    @TableField("menu_id")
    private Long menuId;


    public ManageRoleMenu(Long roleId, Long menuId) {
        this.roleId = roleId;
        this.menuId = menuId;
    }
}
