package cn.kerui.repos.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 
 * @Author yangkai
 * @Date 2023/1/9 16:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable {

    /**
     * 业务主键
     */
    private Long id;

    /**
     * 支付流水号
     */
    private String serial;
}
