package cn.kerui.repos.entities.pub;


import cn.kerui.common.core.BaseQuery;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 *
 * </p>
 *
 * @author yangkai
 * @since 2023-10-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("success_killed")
@ApiModel(value="SuccessKilledEntity对象", description="")
public class SuccessKilledEntity extends BaseQuery {

    private static final long serialVersionUID = 2L;

    @ApiModelProperty(value = "商品订单ID")
    @TableId("skgId")
    private Integer skgId;

    @ApiModelProperty(value = "患者ID")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "状态")
    @TableField("state")
    private Integer state;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;


}
