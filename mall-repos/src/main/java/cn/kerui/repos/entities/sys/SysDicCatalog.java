package cn.kerui.repos.entities.sys;

import cn.kerui.common.core.BaseModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
/**
 * <p>
 * 
 *
 * </p>
 *
 * @author yangkai
 * @since 2024-01-12
 */
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@TableName("sys_dic_catalog")
@ApiModel(value="SysDicCatalog对象", description="")
public class SysDicCatalog extends BaseModel {

    @Serial
    private static final long serialVersionUID = 2L;

    @ApiModelProperty(value = "字典编码")
    @TableId(value = "dic_code", type = IdType.AUTO)
    private String dicCode;

    @ApiModelProperty(value = "字典名称")
    @TableField("dic_name")
    private String dicName;

    @ApiModelProperty(value = "排序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "有效性 0 无效 1 有效")
    @TableField("state")
    private Integer state;

    @ApiModelProperty(value = "扩展数据")
    @TableField("add_info")
    private String addInfo;


}
