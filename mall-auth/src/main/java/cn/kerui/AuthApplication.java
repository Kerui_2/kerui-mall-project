package cn.kerui;

import cn.kerui.common.framework.util.spring.annotation.EnableInjectSpringContext;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p> 认证服务启动类 </p>
 * <p>创建于 2024/1/11 11:21 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@SpringBootApplication
@MapperScan(basePackages = {"cn.kerui.*.mapper"})
@EnableFeignClients
@EnableDiscoveryClient
@EnableInjectSpringContext
public class AuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }
}
