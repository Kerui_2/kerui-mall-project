package cn.kerui.auth.event.vo;

import com.alibaba.fastjson.JSONObject;
import jakarta.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p> 登录日志实体类 </p>
 * <p>创建于 2024/1/4 12:16 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since
 */
@Getter
@Setter
@NoArgsConstructor
public class LoginInfoEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 登录类型， 登录和登出
     */
    private String loginType;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 消息
     */
    private String message;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 登录状态 (0成功 1 失败)
     */
    private Integer status;

    /**
     * 登录方式
     */
    private String grantType;

    /**
     * 请求体
     */
    private HttpServletRequest httpServletRequest;

    /**
     * 消息入参
     */
    private Object[] args;


    /**
     * 请求参数
     */
    private JSONObject params;
}
