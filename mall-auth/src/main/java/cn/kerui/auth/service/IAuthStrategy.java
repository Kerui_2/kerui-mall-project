package cn.kerui.auth.service;

import cn.dev33.satoken.stp.SaLoginModel;
import cn.kerui.auth.vo.LoginVo;
import cn.kerui.common.exception.BaseException;
import cn.kerui.common.framework.util.spring.util.SpringContextUtil;

/**
 * <p> 登录抽象工厂类 </p>
 * <p>创建于 2023/12/25 11:17 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
public interface IAuthStrategy {


    String baseBeanName = "AuthStrategy";

    static LoginVo login(String body, String grantType, SaLoginModel loginModel) {
        // 调用客户端处理业务类
        String beanName = grantType + baseBeanName;
        if (!SpringContextUtil.containsBean(beanName)) {
           throw new BaseException("未获取到对应的登录处理器");
        }
        IAuthStrategy bean = SpringContextUtil.getBean(beanName, IAuthStrategy.class);
        return bean.login(body, loginModel);
    }


    /**
     * 登录
     */
    LoginVo login(String body, SaLoginModel loginModel);

}
