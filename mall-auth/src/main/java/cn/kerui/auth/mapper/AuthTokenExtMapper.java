package cn.kerui.auth.mapper;

import cn.dev33.satoken.stp.SaLoginModel;
import cn.kerui.repos.mapper.auth.AuthTokenRoleMapper;
import org.springframework.stereotype.Repository;

/**
 * <p> Token数据层继承 </p>
 * <p>创建于 2024/1/3 15:02 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Repository
public interface AuthTokenExtMapper extends AuthTokenRoleMapper {

    SaLoginModel getLoginModel(String terminal);
}
