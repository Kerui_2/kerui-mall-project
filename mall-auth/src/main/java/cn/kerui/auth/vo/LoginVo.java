package cn.kerui.auth.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> 登录完成返回值 </p>
 * <p>创建于 2023/12/25 17:56 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginVo {

    /**
     * 授权令牌
     */
    @JsonProperty("access_token")
    private String accToken;

    /**
     * 刷新令牌
     */
    @JsonProperty("refresh_token")
    private String refreshToken;

    /**
     * 授权令牌access_token的过期时间
     */
    @JsonProperty("expire_in")
    private Long expireIn;
}
