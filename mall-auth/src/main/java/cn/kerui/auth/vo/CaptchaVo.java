package cn.kerui.auth.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p> 验证码信息 </p>
 * <p>创建于 2023/12/15 12:13 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class CaptchaVo implements Serializable {

    @Serial
    public static final long serialVersionUID = 1L;

    /**
     * 是否开启验证码
     */
    private Boolean captchaEnabled = true;

    /**
     * 验证码编码
     */
    private String uuid;

    /**
     * 验证码图片
     */
    private String image;
}
