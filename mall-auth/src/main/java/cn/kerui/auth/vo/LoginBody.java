package cn.kerui.auth.vo;

import cn.kerui.common.satoken.enums.LoginTerminal;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> 密码登陆请求体 </p>
 * <p>创建于 2023/12/23 11:37 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class LoginBody {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 验证码uuid
     */
    private String uuid;

    /**
     * 登录端
     */
//    @NotBlank(message = "userTerminal can not empty")
    private LoginTerminal userTerminal;

    /**
     * 登陆类型
     */
    @NotBlank(message = "grantType can not empty")
    private String grantType;
}
