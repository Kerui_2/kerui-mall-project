package cn.kerui.common.enums;

/**
 * <p> 用户分类 </p>
 * <p>创建于 2023/12/25 18:26 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
public enum UserTypeEnum {
    /**
     * 管理员
     */
    ADMIN("admin"),

    /**
     * 普通用户
     */
    USER("user"),

    /**
     * 超级管理员
     */
    SUPER_ADMIN("super_admin");

    String value;

    UserTypeEnum(String value) {
        this.value = value;
    }

    public static UserTypeEnum get(String value) {
        for (UserTypeEnum userTypeEnum : UserTypeEnum.values()) {
            if (userTypeEnum.value.equals(value)) {
                return userTypeEnum;
            }
        }
        return null;
    }
}
