package cn.kerui.common.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * <p> 登录类型枚举 </p>
 * <p>创建于 2023/12/25 14:18 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
public enum LoginType {

    /**
     * 密码
     */
    PASSWORD,

    /**
     * 短信
     */
    SMS,

    /**
     * 邮箱
     */
    EMAIL,

    /**
     * 微信
     */
    WECHAT;

}
