package cn.kerui.common.enums;

import cn.kerui.common.core.enums.BaseEnum;
import lombok.Getter;

/**
 * <p> 菜单类型枚举 </p>
 * <p>创建于 2024/1/8 16:03 </p>
 *
 * @author yangkai
 * @version v1.0
 * @since 1.0.0
 */
@Getter
public enum MenuTypeEnum implements BaseEnum<MenuTypeEnum, String> {

    /**
     * 目录
     */
    M("M"),

    /**
     * 菜单
     */
    C("C"),

    /**
     * 按钮
     */
    F("F");

    private final String value;

    MenuTypeEnum(String value) {
        this.value = value;
    }

    @Override
    public String getSerializableValue() {
        return value;
    }
}
