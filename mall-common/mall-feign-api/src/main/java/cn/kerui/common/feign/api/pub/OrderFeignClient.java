package cn.kerui.common.feign.api.pub;


import cn.kerui.common.core.domain.CommonResult;
import cn.kerui.common.feign.api.pub.impl.OrderFeignClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName OrderFeignClient.java
 * @createTime 2023年10月16日 16:36:00
 */
@FeignClient(value = "cloud-product-service", path = "/product", fallback = OrderFeignClientImpl.class)
public interface OrderFeignClient {

    @GetMapping(value = "/consumer/payment/nacos/{id}")
    CommonResult<String> getPaymentById(@PathVariable("id") Long id);

    @GetMapping("/test")
    CommonResult<String> test();
}
