package cn.kerui.common.feign.api.pub.impl;

import cn.kerui.common.core.domain.CommonResult;
import cn.kerui.common.core.enums.AppHttpCodeEnum;
import cn.kerui.common.feign.api.pub.OrderFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName OrderFeignClientImpl.java
 * @createTime 2023年10月16日 20:08:00
 */
@Component
@Slf4j
public class OrderFeignClientImpl implements OrderFeignClient {
    @Override
    public CommonResult<String> getPaymentById(Long id) {
        return CommonResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
    }

    @Override
    public CommonResult<String> test() {
        log.info("调用异常");
        return null;
    }
}
