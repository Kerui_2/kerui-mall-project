package cn.kerui.minapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName DataSourceConfiguartion.java
 * 
 * @createTime 2023年09月28日 10:16:00
 */
@Configuration
public class DataSourceConfiguration {

    /**
     * 数据源类型
     */
    @Value("${druid.type}")
    private Class<? extends DataSource> dataSourceType;

    @Bean(name = "masterDb")
    @ConfigurationProperties(prefix = "druid.master")
    public DataSource masterDb() {
        DataSource build = DataSourceBuilder.create().type(dataSourceType).build();
        return build;
    }
}
