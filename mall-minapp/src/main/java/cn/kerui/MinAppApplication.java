package cn.kerui;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author yangkai
 * @version 1.0.0
 * @ClassName Payment9001Application.java
 * 
 * @createTime 2023年06月13日 15:18:00
 */
@SpringBootApplication
@MapperScan(basePackages = {"cn.kerui.*.mapper", "cn.kerui.minapp.mapper"})
@EnableFeignClients
@EnableDiscoveryClient
@EnableAdminServer
public class MinAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinAppApplication.class, args);
    }
}
